from random import randint

name = input("Hi! What is your name?")

guess_n = 0

for t in range(5):
    month_guess = randint(1, 12)
    year_guess = randint(1924, 2004)
    guess_n = guess_n + 1
    real_guess_n = "guess" + str(guess_n)



    print(real_guess_n, "were you born in", month_guess, "/", year_guess ,"?")

    reply = input()

    if reply == "yes":
        print("I knew it!!")
        break
    elif reply == "no" and real_guess_n == "guess5":
        print("I have other things to do, bye!")
    elif reply == "no":
        print("Drat! Lemme try again!")
